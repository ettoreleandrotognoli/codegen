package io.gitlab.ettotog.codegen.loader;

import com.fasterxml.jackson.databind.JsonNode;
import io.gitlab.ettotog.codegen.api.CodegenNode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class GenerationNode {
    private final CodegenNode codegen;
    private final JsonNode source;
}
